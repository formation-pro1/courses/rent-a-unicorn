package com.unicorn.services.suggestion;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UnicornSuggestionEndpointTest {

    @Test
    public void testSuggestUnicorn() throws UnicornSuggestionFault {
        var suggestionEndpoint = new SimpleUnicornSuggestionEndpoint();
        var response = suggestionEndpoint.suggestUnicorn(new UnicornSuggestionRequest("white"));
        assertEquals("Sequins", response.getUnicorn());
    }

}
