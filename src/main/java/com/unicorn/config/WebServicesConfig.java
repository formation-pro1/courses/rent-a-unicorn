package com.unicorn.config;

import com.unicorn.services.suggestion.UnicornSuggestionEndpoint;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jakarta.xml.ws.Endpoint;

@Configuration
public class WebServicesConfig {

    @Autowired
    private Bus bus;

    @Value("${publish.url.prefix}")
    private String urlPrefix;

    @Bean
    public Endpoint endpoint(UnicornSuggestionEndpoint unicornSuggestionEndpoint) {
        EndpointImpl endpoint = new EndpointImpl(bus, unicornSuggestionEndpoint);
        endpoint.setPublishedEndpointUrl(urlPrefix + "/services/suggestion");
        endpoint.publish("/suggestion");
        return endpoint;
    }

}
