package com.unicorn.services.suggestion;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import jakarta.jws.WebService;

@Service
@WebService(targetNamespace = "http://rent-a-unicorn.com/services/suggestion",
        serviceName = "UnicornSuggestionEndpointService", portName = "UnicornSuggestionEndpointPort")
public class SimpleUnicornSuggestionEndpoint implements UnicornSuggestionEndpoint {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public UnicornSuggestionResponse suggestUnicorn(UnicornSuggestionRequest request) throws UnicornSuggestionFault {
        logger.debug("UnicornSuggestionRequest with color '{}'", request.getFavouriteColor());
        return new UnicornSuggestionResponse(findUnicorn(request.getFavouriteColor()).orElseThrow(() -> new UnicornSuggestionFault("No suggestion :(")));
    }

    private Optional<String> findUnicorn(String color) {
        Optional<String> unicornName;
        switch (color) {
            case "white":
                unicornName = Optional.of("Sequins");
                break;
            case "yellow":
                unicornName = Optional.of("Cornflakes") ;
                break;
            case "pink":
                unicornName = Optional.of("Cornemuse");
                break;
            case "green":
                unicornName = Optional.of("Célestior");
                break;
            case "black":
                unicornName = Optional.of("Tornado");
                break;
            default:
                unicornName = Optional.empty();
                break;
        }
        return unicornName;
    }

}
