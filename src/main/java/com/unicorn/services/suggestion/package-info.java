@XmlSchema(namespace = "http://rent-a-unicorn.com/services/suggestion",
        elementFormDefault= XmlNsForm.QUALIFIED)
package com.unicorn.services.suggestion;

import jakarta.xml.bind.annotation.XmlNsForm;
import jakarta.xml.bind.annotation.XmlSchema;