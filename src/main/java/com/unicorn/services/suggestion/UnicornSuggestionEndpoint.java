package com.unicorn.services.suggestion;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebResult;
import jakarta.jws.WebService;
import jakarta.xml.bind.annotation.XmlElement;

@WebService(targetNamespace = "http://rent-a-unicorn.com/services/suggestion")
public interface UnicornSuggestionEndpoint {

    @WebMethod(operationName = "SuggestUnicorn")
    @WebResult(name = "UnicornSuggestionResponse")
    @XmlElement(required = true)
    UnicornSuggestionResponse suggestUnicorn(@WebParam(name = "UnicornSuggestionRequest") @XmlElement(required = true) UnicornSuggestionRequest request) throws UnicornSuggestionFault;

}
