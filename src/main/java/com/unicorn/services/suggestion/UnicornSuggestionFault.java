package com.unicorn.services.suggestion;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@AllArgsConstructor
public class UnicornSuggestionFault extends Exception {

    private static final long serialVersionUID = -3890203923092013739L;
    
	@XmlElement(name = "Message", required = true)
    private String message;

}
