package com.unicorn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnicornWebservicesApp {

    public static void main(String[] args) {
        SpringApplication.run(UnicornWebservicesApp.class, args);
    }

}
