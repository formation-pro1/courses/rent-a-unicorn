package com.unicorn.api;

import com.unicorn.api.model.Unicorn;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/unicorn")
public class UnicornController {

    @GetMapping
    public List<Unicorn> findAll() {
        Unicorn unicorn1 = new Unicorn();
        unicorn1.setId(1);
        unicorn1.setName("Cornemuse");

        Unicorn unicorn2 = new Unicorn();
        unicorn2.setId(2);
        unicorn2.setName("Cornflakes");
        return Arrays.asList(unicorn1, unicorn2);
    }
}
