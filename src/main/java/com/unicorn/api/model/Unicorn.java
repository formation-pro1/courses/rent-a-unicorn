package com.unicorn.api.model;

import lombok.Data;

@Data
public class Unicorn {
    private Integer id;
    private String name;
}
