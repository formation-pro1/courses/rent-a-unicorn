# SOAP & Rest example

## Run with Maven (JDK21 required)

```shell
mvnw spring-boot:run
```

Open `http://localhost:8080/services/suggestion?wsdl`
Open `http://localhost:8080/swagger-ui/index.html` and explore `/v3/api-docs`

